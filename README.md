# site_sid


# Pré-requis

### télécharger git : https://git-scm.com/downloads

### ouvrir un git-bash

### se rendre à l'emplacement où tu veux avoir ton projet (exemple : cd Documents)

### importer le dossier avec la commande git clone https://gitlab.com/Julienformaticien/site_sid.git


# Pour importer les dernière modif :

### ouvrir git bash
### utiliser la commande git pull
### si la commande ne fonctionne pas essayer : git pull origin master

# Pour envoyer tes dernières modif:

### ouvrir git bash
### utiliser dans l'ordre les commande :
### git add .
### git commit -am "nom du commit" (exemple : git commit -am "ajout page d'acceuil")
### git push
### si la commande ne fonctionne pas essayer : git push origin master