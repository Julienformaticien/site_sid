#Différents imports utiles lors de notre projet
import numpy as np
from sqlalchemy import create_engine
import cx_Oracle
import datetime
from flask_mysqldb import MySQL
import MySQLdb.cursors
from flask import jsonify

from flask import Flask, render_template, Markup, request, session, redirect, url_for
app = Flask(__name__)
mysql = MySQL(app)

#Etablisemment de la connection avec la base de données 
engine = create_engine('oracle://cnj3031a:Yoloswag69@telline.univ-tlse3.fr:1521/etupre')






    
"""Gestion de la page d'accueil sur notre site ,si une session est en cours alors on renvoi directement
vers la page du joueur , sinon on affiche le page html relative à l'accueil  """

@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('accueil.html')
    else:
        return redirect(url_for('jeu',identifiant=session.get('username')))





"""Gestion de la page login sur notre site ,si une session est en cours alors on renvoi directement
    vers la page du joueur sinon , on attend que le joueur remplisse le formulaire de connection  """

@app.route('/login', methods=['GET', 'POST'])
def login():
    if not session.get('logged_in'):
    
        msge = ''
        if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
            username = request.form['username']
            password = request.form['password']
            #On recherche si l'username et le password donnés se trouvent bien dans la BD
            with engine.connect() as con:
                rs=con.execute('SELECT * FROM sbc2937a.Joueur WHERE identifiant = \''
                                + username + '\' AND mdp = \''
                                + password + '\'')
                for row in rs:
                    #Si c'est le cas , on considère que l'utilisateur est connecté et on enregistre son identifiant
                    session['logged_in'] = True
                    session['username'] = username
                    #Puis on le renvois automatiquement vers sa page Joueur
                    return redirect(url_for('jeu',identifiant=username))
            #Si l'identifiant et le mdp ne se trouvent pas dans la bd on actualise le message à afficher
            msge='Identifiant ou mdp incorect'
    else :
        #Si l'utilisateur était déja connecté , on le renvoi à sa page joueur
        return   redirect(url_for('jeu',identifiant=session['username']))
    return render_template('login.html',msg=msge)  






"""Si l'utilisateur se deconnecte , on supprime l'identifiant enregisté dans session
            et on renvois la page d'accueil """ 


@app.route('/logout')
def logout():
    session.pop('username', None)
    session.pop('logged_in', None)
    return render_template('accueil.html')





"""La route jeu représente la page du joueur , c'est pour cela que la route prend en paramètre un identifiant 
Chaque route /jeu/identifiant présentera des informations différentes 
On test d'abord si l'utilisateur est connecté sous le même identifiant que celui passé dans l'url afin d'éviter toute usurpation
Si c'est le cas on affiche les informations utiles eu joueur 
""" 

@app.route('/jeu/<identifiant>', methods=['GET', 'POST'])
def jeu(identifiant="None"):

    if session.get('logged_in') and identifiant ==  session['username'] :
        connection = engine.raw_connection()
        cursor = connection.cursor()
        temps = cursor.var(cx_Oracle.NUMBER) 
        cursor.callproc("sbc2937a.tempsblocage", [session['username'],temps]) #Calcul puis affichage du nombre de minutes restantes avant de pouvoir rejouer
        if int(temps.values[0])>0:
            msge_temp="Blocage encore actif pendant : " +str(temps.values[0]) +" minutes"
        else :
            msge_temp="Pas de blocage actif"

        score = calcul_score() #Calcul du score journalier pour le Joueur ( multiniveau)
        return render_template("accueil2.html",name=identifiant,tempsm=msge_temp,scoreJ=score)
    else :
        #Si le joueur tente d'accéder aux infos du joueur sans être connecte on lui renvoi le template de la page d'accueil
       return render_template('accueil.html')




"""La fonction calcul_score permet de calculer le score journalier multi-niveau d'un joueur 
Pour le calcul du score nous utilisons un système de ranking , ou chaque partie gagné lui rapport un total de points égale au score
et chaque partie perdue lui retire 500 points peu importe le niveau , le total de points est toujours supérieur ou égale a 0"""       

def calcul_score():
    scoreJoueurTotal = 0
    scoreJoueur =0
    scoreNeg=0

    with engine.connect() as con: 
        rs=con.execute(' select sum(score) from sbc2937a.parties where etat is not null and identifiantp = \'' + session['username'] + '\' and  TO_CHAR(DATE_PARTIE,\'DD-MM-YYYY \') = TO_DATE((\''+str(datetime.datetime.today().strftime('%d/%m/%Y'))+'\'))    ' )
        for row in rs:
            scoreJoueur = row[0]
            
        rs2=con.execute('select count(*),niveaup from sbc2937a.parties where identifiantP=\''+ session['username'] + '\' and (etat=0 or etat is null) and  TO_CHAR(DATE_PARTIE,\'DD-MM-YYYY \') = TO_DATE((\''+str(datetime.datetime.today().strftime('%d/%m/%Y'))+'\')) group by niveaup order by niveaup asc')
        for row2 in rs2 :
            scoreNeg=scoreNeg+row2[0]*500
        if row[0] !=None:
            if scoreNeg > row[0]: 
                scoreJoueurTotal = 0
            else :
                scoreJoueurTotal = scoreJoueur -scoreNeg
        else :
            scoreJoueurTotal = 0
    return scoreJoueurTotal




""" Gestion de la page d'inscription sur notre site ,si une session est en cours alors on renvoi directement
    vers la page du joueur concerné  sinon on affiche un formulaire au visiteur 
    après avoir reçu les informations sur formulaire d'inscription , on traites les cas possibles d'erreurs et on affiche un message
    en conséquence . Si il n'y a pas d'erreurs , on appel la procédure Insert_Joueur et on renvoi vers la page du joueur nouvellement inscrit  """   

@app.route('/inscription', methods=['GET', 'POST'])
def inscription():
    if session.get('logged_in'):
        return redirect(url_for('home'))
  
    message=''
    #Réception du formulaire d'inscription
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        connection = engine.raw_connection()
        cursor = connection.cursor()
       
        retour = cursor.var(cx_Oracle.NUMBER) 
        #Appel de la Procédure insertJoueur
        cursor.callproc("sbc2937a.Insert_Joueur", [username,password,retour])
        if retour.values[0]==0:
            message="Félicitation vous êtes inscrits"
            session['logged_in'] = True
            session['username'] = username
            #Redirection automatique après inscription
            return redirect(url_for('jeu',identifiant=username))
        #Différentes erreurs possibles
        if retour.values[0]==1:
            message="Ce nom d'utilisateur est déja utilisé"
        if retour.values[0]==2:
            message ="La taille minimale du mdp est 6"

       
    return render_template('register.html',msg=message)
 



""" Gestion de la route mastermind 
Des radio boutons de niveau sont générés côté python ainsi qu'un bouton de validation qui contient l'appel à la fonction js

"""   


@app.route("/mastermind")
def mastermind():

    if session.get('logged_in'):
        code_html = ""
        code_html += '<form id ="levels">\
        <input type="radio" id="lvl1" name="level" value="1"\
            checked>\
            <label for="lvl1">Niveau facile</label>\
            <input type="radio" id="lvl2" name="level" value="2">\
            <label for="lvl2">Niveau moyen</label>\
            <input type="radio" id="lvl3" name="level" value="3">\
            <label for="lvl3">Niveau difficile</label>\
        </form>'
        code_html += "<button type=\"button\" id='back' onclick=\"newGame()\">Create Game</button>"
        
        return render_template("mastermind.html", content=Markup(code_html))
        
    return redirect(url_for('login'))
    




""" 
La route mastermindContent permet de recevoir et renvoyer des requêtes ajax au js 
tout d'abord on récupère le niveau de la partie qu'on souhaite creer 
on vérifie qu'il n' y as pas d'interdiction de  jouer (trigger limitationpartie et niveauaccessible)
le cas échant , on appel la procédure Creerpartie qui insert la partie dans la bd ainsi que la solution 
Après avoir creer la partie , on récupère toutes les informations necessaires pour que le js 
reproduisent le jeu (tailleX,tailleY , les couleurs possibles  etc...)
"""   

@app.route('/mastermindContent',methods=[ 'POST'])
def getContent() :

    if (request.method == 'POST'):
        niveau = request.json
        #Dictionnaire permettant de traduire les couleurs en anglais (utile pour le js)
        dicto={"bleu":"blue" ,"vert":"green","rouge":"red","noir":"black","blanc":"white","rose":"pink","marron":"brown","violet":"purple"}  
        listecouleursniveau = []
        listesolution = []
        msge=''
        niveaudebloquer=0
        connection = engine.raw_connection()
        cursor = connection.cursor()
        retour = cursor.var(cx_Oracle.NUMBER) 
        idpar = cursor.var(cx_Oracle.NUMBER)
        cursor = connection.cursor()
        temps = cursor.var(cx_Oracle.NUMBER) 
        cursor.callproc("sbc2937a.tempsblocage", [session['username'],temps]) #On vérifie qu'il n'y ai pas de blocage en cours
        #Test si le joeur est en droit de lancer cette partie
        with engine.connect() as con:
            niveaurequete=con.execute('SELECT max(niveaup) FROM sbc2937a.parties WHERE identifiantp = \''
                             + session['username'] + '\' and etat=1 ' )
            for row in niveaurequete:
                if str(row[0]) in ('1','2','3'):
                    niveaudebloquer=row[0]
                    
        if int(niveaudebloquer)+1<int(niveau) :
            print('erreur_niveau')
            return jsonify('erreur_niveau')
        if temps.values[0]>0 :
            print('erreur_temps',)
            return jsonify(['erreur_temps',temps.values[0]])
        cursor.callproc("sbc2937a.creerPartie", [session['username'],niveau,idpar,retour])
        #Récupération des informations necessaires pour dessiner le jeu 
        with engine.connect() as con:
            rs=con.execute('SELECT tailleX,tailleY,nbrecouleurs FROM sbc2937a.difficulte WHERE niveau = \''
                             + str(niveau) + '\' ' )
            for row in rs:
                #Récupération des dimensions
                tailleX_ = row[0]
                tailley_ = row[1]
                nbrecouleurs_ = row[2]

            rs2=con.execute('SELECT COULEUR FROM sbc2937a.billes WHERE numero <= \''
                             + str(nbrecouleurs_) + '\' order by numero asc ' )
            for row in rs2 :
                listecouleursniveau.append(dicto[row[0]])

            rs3=con.execute('SELECT couleurs FROM sbc2937a.solution WHERE idpartie = \''
                             + str(int(idpar.values[0])) + '\' order by ordre asc  ' )
            for row in rs3:
                print(" sa marche")
                listesolution.append(dicto[row[0]])
        #Renvois des informations necessaires au format json
        idP = idpar.values[0]
        content={
        "idP" : idP,
        "solution" : listesolution,
        "colors"  : listecouleursniveau,
        "tailleY" : tailley_,
        "tailleX" : tailleX_
        }
         
    return jsonify(content)



""" 
La route mastermindReplayContent permet de recevoir et renvoyer des requêtes ajax au js 
Elle récuperer l'identifiant d'une partie 
Elle renvoie les informations nécessaires pour rejouer une partie :
La configuration (TailleX tailleY de la grille , nombre de couleurs, etc ....)
Les coups Joués ainsi que leurs ordres

"""   

@app.route('/mastermindReplayContent',methods=['POST'])
def getReplayContent() :
    if (request.method == 'POST'):
        dicto={"bleu":"blue" ,"vert":"green","rouge":"red","noir":"black","blanc":"white","rose":"pink","marron":"brown","violet":"purple"} 
        #Idpartie envoyé depuis la javascript
        idpartie = request.json
        listecouleursniveau = []
        listesolution = []
        listecoups = []
        #Récupération de toutes les informations importantes pour rejouer une partie
        with engine.connect() as con:
            rs=con.execute('SELECT tailleX,tailleY,nbrecouleurs FROM sbc2937a.difficulte,sbc2937a.parties WHERE niveau = niveaup and idpartie= \''
                             + str(idpartie) + '\' ' )
            for row in rs:
                #Dimensions de la grille
                tailleX_ = row[0]
                tailley_ = row[1]
                nbrecouleurs_ = row[2]

            rs2=con.execute('SELECT COULEUR FROM sbc2937a.billes WHERE numero <= \''
                             + str(nbrecouleurs_) + '\' order by numero asc ' )
            for row in rs2 :
                #Liste des couleurs que le joueur peut placer
                listecouleursniveau.append(dicto[row[0]])

            rs3=con.execute('SELECT couleurs FROM sbc2937a.solution WHERE idpartie = \''
                             + str((idpartie)) + '\' order by ordre asc  ' )
            for row in rs3:
                #La solution que le joueur doit trouver
                listesolution.append(dicto[row[0]])
            
            rs4=con.execute('SELECT ligne,colonne,couleurB FROM sbc2937a.coups WHERE idpartie = \''
                             + str((idpartie)) + '\' order by ordre asc  ' )
            for row in rs4 :
                #Les coups  avec la ligne colonne couleur et ordre
                listecoups.append([str(row[0]-1)+str(row[1]-1),dicto[row[2]]])
        
        content={
        "solution" : listesolution,
        "colors"  : listecouleursniveau,
        "coups" : listecoups,
        "tailleY" : tailley_,
        "tailleX" : tailleX_
    }


    return jsonify(content) 




""" 
La route lastAction permet de recevoir et renvoyer des requêtes ajax au js concernant les coups joués
On récupère les informations sur les coups joués depuis le javascript
et on insere le coup via la procédure JouerCoup, Ici aucune exception n'est à traiter
car notre jeu empeche de jouer des coups interdits
"""   


@app.route('/lastAction',methods=['POST'])
def sendActionToDB():
    if (request.method == 'POST'):
        dicto={"blue" :"bleu","green":"vert","red":"rouge","black":"noir","white":"blanc","pink":"rose","brown":"marron","purple":"violet"} 
        #Réception des informations relatives à un coup
        coup = request.json
        idP = coup[0]
        color = coup[1]
        position = coup[2]
        line = position[3]
        column = position[4]

        
        connection = engine.raw_connection()
        cursor = connection.cursor()
        retour = cursor.var(cx_Oracle.NUMBER)
        #Appel de la procédure qui insere le coup 
        cursor.callproc("sbc2937a.JouerCoup", [idP,int(line)+1,int(column)+1,dicto[color],0,retour])
        status = 'SUCCESSFUL' 
        #Renvois du statut
        return jsonify(status)




""" 
La route GameState permet de recevoir et renvoyer des requêtes ajax au js concernant le statut d'une partie à sa fin
on récupère le statut de la partie (gagné ou perdue)
puis execute la procédure updatePartie afin de mettre à jour l'etat de la partie
0 = perdue / 1 = gagné
On peut noté que les informations renvoyé par le js ne sont pas utilisés car notre procédure updatepartie
détermine elle me si la partie est gagné ou perdue
"""   


@app.route('/gameState',methods=['POST'])
def isWon():
    #Réception des données relatives à l'état d'une partie
    if (request.method == 'POST'):
        state = request.json[0]
        idP = request.json[1]
        connection = engine.raw_connection()
        cursor = connection.cursor()
        #Initialisation des variables retours pour la procédure updatePartie
        retour = cursor.var(cx_Oracle.NUMBER)
        score = cursor.var(cx_Oracle.NUMBER)
        #Mise à jour de l'état et du score d'une partie lorsqu'elle est finie
        cursor.callproc("sbc2937a.UpdatePartie", [idP,score,retour]) 


    #Renvoi du score au format json
    return jsonify(score.values[0])




""" 
La fonction recoveGame permet dde récupérer l'ensemble des informations sur les parties joués par un utilisateur donné
On récupère  l'id de l'utilisateur en question via les informations sur la session et on interroge les données de la base
ici nous avons décider de  récupérer seulement les parties gagnés et les parties ou le joueur est arrivé jusqu'à la dernière ligne 

"""   

def RecoveGame() :
    #Récupération de l'identifiant du jouer concerné
    idJoueur = session['username']
    listePartie = []
    with engine.connect() as con:
            #Interrogation de la base de données
            rs = con.execute('SELECT distinct p.IDPARTIE,niveauP,Score,Etat,date_partie FROM sbc2937a.Parties p, sbc2937a.Coups c WHERE p.idpartie=c.idpartie and IDENTIFIANTP = \''
                             + str((idJoueur)) + '\'and (p.etat = 1 or ( c.ligne =(select d.tailleY from sbc2937a.difficulte d ,sbc2937a.parties p2 where  d.niveau = p2.niveauP and p2.idpartie =p.idpartie   ))) order by IDPARTIE DESC ')
            for row in rs:
                listePartie.append([row[0],row[1],row[2],row[3],row[4]])
    #Renvoi des informations de chaque parties sous la forme d'une liste de liste
    return listePartie





""" 
La route replays permet de récupérer les informations relatives aux parties d'un joueur 
puis de les afficher sur le site via une injection  html
on affiche par ailleur le score du joueur

"""    

@app.route('/replays')
def replays():
    if session.get('logged_in'):
        #Récupération de la liste des parties et leurs informations
        list_parties = RecoveGame()
        difficulty = ''
        score = calcul_score()



        code_html = "<h1 id='head'>VOS PARTIES</h1> <h1 id='head'>Votre score total du jour est : "+str(score)+"</h1> <table>"
        code_html += "<td><button type=\"button\" onclick=\"window.location.href='/mastermind';\"> NOUVELLE PARTIE </button></td>"
        #Pour chaque partie on insère une balise tr content une balise button cliquable et qui affiche les informations d'une partie
        #Le clique sur le boutton permet d'appeler une fonction js qui rejouera la partie en question
        for atr in list_parties:
            score = '+'+str(atr[2])

            if str(atr[3])=='0':
                score = '-500'

            if str(atr[1])=='1':
                difficulty = 'facile'
            elif str(atr[1])=='2':
                difficulty = 'moyenne'
            elif str(atr[1])=='3':
                difficulty = 'difficile'

            code_html += "<tr  class='replays'><td ><button type=\"button\" class='status-"+str(atr[3])+"' onclick=\"replayGame("+str(atr[0])+")\">Partie "+ difficulty +" joué le "+ str(atr[4]) +" SCORE "+score+"</button></tr>"
    # code_html +=    +" "+ str(list_parties[0][2]) +" "+ str(list_parties[0][3])


        code_html += "</table>"
        
        
        return render_template("mastermind.html", content=Markup(code_html))
    return redirect(url_for('login'))
 






""" 
La route classement permet de récupérer et d'afficher le classement des joueurs 
elle prend un paramètre niveau pour afficher le classement relatif à ce niveau et un paramètre daily qui affiche
le classement journalier multiniveau .
Le classement est calculé selon un système de ranking ou chaque partie gagné remport un nombre de point égale au score
et chaque partie perdue enlève un nombre de point égale a 500 peu importe le niveau 
"""   


  
@app.route("/classement/<niveau>")
def classement(niveau):
    listejoueur = []
    listescore= []
    code_html=''
    if niveau =='daily':
        with engine.connect() as con:
                #Premier classement en fonction de la valeur du score totale des parties gagnés 
                rs=con.execute(' select identifiantP,sum(score) from sbc2937a.parties where etat is not null and  TO_CHAR(DATE_PARTIE,\'DD-MM-YYYY \') = TO_DATE((\''+str(datetime.datetime.today().strftime('%d/%m/%Y'))+'\'))   group by identifiantP ' )
                for row in rs:
                    scoreNeg = 0
                    rs2=con.execute('select count(*),niveaup from sbc2937a.parties where identifiantP=\''+ str(row[0]) + '\' and (etat=0 or etat is null) and  TO_CHAR(DATE_PARTIE,\'DD-MM-YYYY \') = TO_DATE((\''+str(datetime.datetime.today().strftime('%d/%m/%Y'))+'\')) group by niveaup order by niveaup asc')
                    #On retire ensuite 500 points pour chaque partie perdue
                    for row2 in rs2 :
                        scoreNeg=scoreNeg+row2[0]*500
                    if scoreNeg>row[1]:

                        listejoueur.append(row[0])
                        listescore.append(0)
                    else :
                        listejoueur.append(row[0])
                        listescore.append(row[1]-scoreNeg)
    if niveau in ('1','2','3'):
        #Calcul du classement génerale relatif à un niveau 
        with engine.connect() as con:
                rs=con.execute(' select identifiantP,sum(score) from sbc2937a.parties where etat is not null and  niveaup =\''+ niveau + '\'  group by identifiantP ' )
                for row in rs:
                    print(row[0])
                    scoreNeg = 0
                    
                    rs2=con.execute('select count(*) from sbc2937a.parties where identifiantP=\''+ str(row[0]) + '\' and niveaup=\''+ niveau + '\' and etat=0  ')
                    for row2 in rs2 :
                        scoreNeg=scoreNeg+row2[0]*500
                        print("scoore neg ",scoreNeg)
                    if row[1] !=None :
                        if scoreNeg>row[1]:

                            listejoueur.append(row[0])
                            listescore.append(0)
                        else :
                            listejoueur.append(row[0])
                            listescore.append(row[1]-scoreNeg)
                    else :
                        listejoueur.append(row[0])
                        listescore.append(0)

    #Classement des  joueurs en fonction de leur nombre de point total grace a la librairie numpy
    Idd = np.argsort(listescore,axis=0)

    Xdd = np.asarray(listejoueur)[Idd] #Liste des joueurs organisés
    Ydd = np.asarray(listescore)[Idd] # Lises des scores organisés

    print(Xdd[::-1],Ydd[::-1])

    Xdd= Xdd[::-1]
    Ydd = Ydd[::-1]

    title = 'Classement'
    
    if niveau == '1':
        title+= ' global niveau facile'
    elif niveau == '2':
        title+= ' global niveau moyen'
    elif niveau == '3':
        title+= ' global niveau difficile'
    elif niveau == 'daily':
        title+= ' du jour'

    #Génération du menu deroulant permettant de selectionner le type de classement
    code_html+='<h1 id="title">'+title+'</h1>\
        <select id="list_scores" >\
            <option value="">Changer de classement</option>\
            <option value="daily">Classement du jour</option>\
            <option value="1">Classement global niveau facile</option>\
            <option value="2">Classement global niveau moyen</option>\
            <option value="3">Classement global niveau difficile</option>\
          </select>'
    code_html+="<table>"
    #Génération du classement en fonction du nombre de joueur 
    for i in range(len(Xdd)):
        code_html+="<tr><td>"+str(Xdd[i])+" "+ str(Ydd[i])+"</td></tr>"
    code_html += "</table>"
    #renvoi du templace classement avec injection de html
    return render_template("classement.html",content=Markup(code_html))



if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.run(debug='on')


    	




