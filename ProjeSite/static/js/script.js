//cette fonction permet de lancer une nouvelle partie avec le niveau sélectionné sur la page

function newGame(){

    
    $( "#back" ).remove();
    var niveau = $('input[name=level]:checked', '#levels').val();//on recupere le niveau
    $( "#levels" ).remove();
    // on retire les boutons pour eviter de lancer plusieurs partie en meme temps

    //on fait une requete ajax en POST sur la route mastermindContent afin d'envoyer le niveau 
    $.ajax({
        url: '/mastermindContent',
        dataType: 'json',
        type: 'POST',
        data: JSON.stringify(niveau),
        contentType: 'application/json;charset=UTF-8',
            success: function(data) {
                //en cas de reussite on recupere les info venant de flask sous forme de json dans data
                

                if(data[0] == 'erreur_temps'){//erreur si le joueur ne peut pas jouer pour le moment
                    console.log(data[0]);
                    alert('Vous ne pouvez pas jouer pour le moment.\nTemps restant avant de pouvoir jouer : '+data[1]+' minutes');
                    window.location.replace('/');//on redirige vers le home
                }else if(data=='erreur_niveau'){// erreur si on a pas encore acces au niveau selctionné
                    alert("Vous n'avez pas encore accès à ce niveau.");
                    location.reload();//onrecharge la page pour reafficher les boutton
                }
                    

                else{
                    //sinon on recupere les donnés necessaire au demarrage de la partie

                    var listeCouleurs = data["colors"];
                    var soluce = data["solution"];
                    console.log(soluce);
                    var idP = data["idP"];
                    var tailleX = data["tailleX"];
                    var tailleY = data["tailleY"];
    
         
                    startGame(listeCouleurs,soluce,tailleX, tailleY,idP);//on lance la game grace aux info recuperees

                }


            }, error: function() {//si la requete ajax echoue on recharge
                alert('Une erreur s\'est produite veuillez réessayer');
                location.reload();
            }
    });
    

}


//cette fonction permet de rejouer une partie deja joué
function replayGame(idP){
    $( "table" ).remove(); //on retire les elements de la page
    $("#head").text('Partie n°'+idP);
    $('#move').append('<button class="back" id="reload">Back to replays list</button>');//on ajoute un bouton qui recharge la page pour reafficher la liste des replays
    $('#reload').click(function() {
        location.reload();
    });
    $.ajax({
  url: '/mastermindReplayContent',
  dataType: 'json',
  type: 'POST',
  data: JSON.stringify(idP),//requete ajax qui envoie l'id de la partie afin de recuperer les info de la partie
  contentType: 'application/json;charset=UTF-8',
  success: function(data) {
     var listeCouleurs = data["colors"];
     var soluce = data["solution"];
     var coups = data["coups"];
     var tailleX = data["tailleX"];
     var tailleY = data["tailleY"];
     startReplay(listeCouleurs,soluce,tailleX, tailleY,coups)//on lance le replay grace aux info recuperees
  }, error: function() {
     alert('Une erreur s\'est produite');
     location.reload();
  }
});

}




//fonction qui affiche le tableau de jeu grace aux taille en entree et aux couleurs dispo
function printGameboard(colors,tailleX, tailleY){
    



    var colorsTable = $('<table></table>').addClass('colors');
    for(var i=0;i<colors.length;i++){
        colorsTable.append('<td><button class="color" id="'+colors[i]+'" style="background-color:'+colors[i]+'"></button></td>');
    }


    $('#colors').append(colorsTable); // on ajoute le tableau des couleurs



    var table = $('<table></table>').addClass('gameboard');
    for (var i = 0; i < tailleY; i++) {
            row = $('<tr></tr>');
            for (var j = 0; j < tailleX; j++) {
                var rowData = $('<td><button class="guessButton" disabled id=btn'+i+j+'></button></td>');


                row.append(rowData);
            }
            row.append('<td> <input type="text" id="hints'+i+'" disabled> </td>');
            table.append(row);
        }
    $('#gameboard').append(table); // on ajoute le tableau de jeu

    var solution = $('<table></table>').addClass('tableSoluce');
    var rowSol = $('<tr></tr>').addClass('show');
    for(var i=0;i<tailleX;i++){
        rowSol.append('<td><button class="solution" id="sol'+i+'"  disabled></button></td>');
    }
    solution.append(rowSol);


    $('#soluce').append(solution); // on ajoute la solution caché


}

function returnFinalGameState(status, idP){//fonction qui renvoie l'etat de la partie gagné perdu et affiche le score gagné
    console.log(status);

    $.ajax({
        type : "POST",
        url : '/gameState',
        dataType: "json",
        data: JSON.stringify([status,idP]),
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            $('#score').append('Votre score est de : '+ data);
        }
    });

}

function showSoluce(soluce){//fonction qui affiche la soluce
    for(var i=0;i<soluce.length;i++){
        document.getElementById('sol'+i).style.backgroundColor=soluce[i];
    }

}




//fonction qui demarre la partie
function startGame(colors,soluce,tailleX, tailleY,idP){
    $(window).bind("beforeunload",function(event) {//empeche le joueur de quitter avec un pop up
        return "Si vous quittez vous allez perdre 500 points de score";//----ne marche pas-----
    });

    



    printGameboard(colors,tailleX, tailleY);// on lance la fonction qui affiche le jeu
    $('#check').append('<button class="checkButton">CHECK</button>'); // on ajoute un boutton pour verifier la proposition

    var nbTry = 0; //nb d'essais

    for (var j = 0; j < tailleX; j++) {
        document.getElementById('btn'+nbTry+j).removeAttribute("disabled");// on  active la premiere ligne
            
    }

    
    var currentCol = '';


    $(".color").click(function(event){

        currentCol = $(event.target).attr("id");//on recupere la couleur du boutton choisi
    });

    $(".guessButton").click(function(event){ // on change la couleur quand un boutton est cliqué
        
        if(currentCol !=$(event.target).attr("name")){
            $(event.target).css("background", currentCol);
            $(event.target).attr("name",currentCol);
            coup = [idP,currentCol,$(event.target).attr("id")];
            $.ajax({
                type : "POST",
                url : '/lastAction',
                dataType: "json",
                data: JSON.stringify(coup),
                contentType: 'application/json;charset=UTF-8',
                success: function (data) {
                    
                }
            });
        }
        




    });


    



    $(".checkButton").click(function(event){ // quand le bouton es cliqué on verifie la proposition
        var proposition = new Array(soluce.length);
        

        for (var j = 0; j < proposition.length; j++) {
            proposition[j] = document.getElementById('btn'+nbTry+j).getAttribute("name");


        }

        if(!proposition.includes(null)){//si la proposition est pleine on peut  commencer a verif

            for (var j = 0; j < proposition.length; j++) {
                document.getElementById('btn'+nbTry+j).setAttribute("disabled","disabled");// on active la ligne suivante

            }
            
            var nbBp =0;
            for (var i = 0; i < proposition.length; ++i) {//on compte le nb de bien placé
            
                if (proposition[i] == soluce[i]){
                    nbBp++;

                }
                
            }

            if(nbBp!=soluce.length && nbTry<tailleY-1){// si ce n'est pas la derniere ligne on compte le nombre de bien palcé
                nbMp = 0;
                
                copySoluce= soluce.slice(0);




                for(var i=0; i<proposition.length;i++){
                    if(proposition[i]==soluce[i]){
                        delete copySoluce[i];
                        delete proposition[i];
                        

                    }

                    
                }

                index=0;
                while(index < proposition.length){
                    for(var j=0;j<copySoluce.length;j++){
                        if(typeof proposition[index]=== 'string' && index<proposition.length && proposition[index]==copySoluce[j]){

                            nbMp++;
                            delete copySoluce[j];
                            j=0;
                            index++;
                        }

                    }
                    index++;
                }

                



                document.getElementById("hints"+nbTry).value = nbBp+' bien placé(s) '+nbMp+' mal placé(s)'; // on affiche les biens placés et mals placés


                nbTry++;//on augmente le nb d'essais
                
                for (var j = 0; j < tailleX; j++) {
                    document.getElementById('btn'+nbTry+j).removeAttribute("disabled"); // on desactive la ligne en cours
                

                }


                
            }else{

                showSoluce(soluce); //sinon on affiche la solution



                if(nbBp==soluce.length){  // on affiche gagné 
                    alert('GAGNÉ');
                    returnFinalGameState(1,idP);

                }else{//sinon perdu
                    alert('PERDU');
                    returnFinalGameState(0,idP);
                }

                    
            }
        }else{

            document.getElementById("hints"+nbTry).value = 'Veuillez placer '+tailleX+' pions'; // sinon on affiche qu'il faut placer tous les pions
        }

    });



}



//fonction qui demarre le replay
function startReplay(colors,soluce,tailleX, tailleY, coups){
    
    printGameboard(colors,tailleX, tailleY); // on affiche le tableau de jeu
    nbCoups = 0;
    $('#check').append('<button class="checkButton">NEXT MOVE</button><br>'); // on ajoute un boutons qui met le coup suivant
    


    var solution = $('<tr class="hidden" style="display: none"></tr>'); // on ajoute la solution


    solution.append('<td>Hover to show solution</td>');
    


    $('.tableSoluce').append(solution);

    //on fait en sorte qu'elle s'affiche quand on passe la souris dessus
    showSoluce(soluce);
    $(".hidden").show();
    $(".show").hide();

    $(".tableSoluce").on('mouseenter', function() {
      $(".hidden").hide();
      $(".show").show();
    })

    $("#soluce").on('mouseleave', function() {
      $(".hidden").show();
      $(".show").hide();
    });








    //on affiche le coup suivant si c'est fini on affiche la solution
    $(".checkButton").click(function(event){
        if(nbCoups<coups.length){
            document.getElementById('btn'+coups[nbCoups][0]).style.backgroundColor=coups[nbCoups][1];
            nbCoups++;
            
        }else{
            $(".hidden").hide();
             $(".show").show();
        }


    });

}


