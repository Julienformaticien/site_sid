DROP TABLE ENSEIGNANT;
DROP TABLE client;
DROP TABLE COMMANDE;
DROP TABLE DETAIL;
DROP TABLE DIFFICULTE;
DROP TABLE EFFFORMATION;
DROP TABLE MATIERE;
DROP TABLE PRODUIT;
DROP TABLE SALLE;
DROP TABLE statut;
DROP TABLE VENDEUR;


DROP TABLE COUPS;
DROP TABLE SOLUTION;
DROP TABLE billes;
DROP TABLE Difficulte ; 
DROP TABLE Parties;
DROP TABLE Joueur;



set SERVEROUTPUT on;


select * from parties where identifiantp = 'Bran' order by idpartie;



CREATE TABLE Joueur (
Identifiant VARCHAR(30),
Mdp VARCHAR(30),
DateInscription DATE,
Constraint pk_Joueur PRIMARY KEY(Identifiant) ,
CONSTRAINT Joueur_Mdp_Length CHECK (LENGTHB(Mdp) >=6)
)


CREATE TABLE Difficulte(
Niveau NUMBER(2) ,
TailleX number(2),
TailleY number(2),
NbreCouleurs NUMBER (2),
Constraint pk_Difficulte PRIMARY KEY(Niveau) 
)


Create table Billes (
Couleur VARCHAR(30),
Lien Varchar(50),
numero number,
Constraint pk_billes PRIMARY KEY (Couleur)


);


Create table Parties(
IdPartie NUMBER(10),
Score Number(10),
Date_Partie Date ,
Etat number (1),
IdentifiantP VARCHAR(30),
NiveauP number(3),
Constraint pk_Parties PRIMARY KEY (IdPartie),
constraint fk_Parties_Identifiant foreign key (IdentifiantP) references Joueur(Identifiant),
constraint fk_Parties_Niveau foreign key (NiveauP) references  Difficulte(Niveau)

);



CREATE TABLE Coups(
ligne NUMBER(2) ,
colonne number(2),
ordre number(3),
temps NUMBER (2),
IdPartie NUMBER(10),
CouleurB  VARCHAR(30),
Constraint pk_Coups PRIMARY KEY(ordre,IdPartie) ,
Constraint fk_coups_IdPartie FOREIGN KEY (Idpartie) references Parties (IdPartie),
Constraint fk_coups_CouleurB FOREIGN KEY (CouleurB) references Billes (Couleur)


)


CREATE TABLE Solution (
Couleurs varchar(30),
idPartie NUMBER(10),
ordre number,
constraint pk_Solution PRIMARY KEY (Couleurs,idPartie,ordre) ,
constraint fk_SOlution_Couleurs FOREIGN KEY ( Couleurs) references Billes (couleur) , 
constraint fk_SOlution_idPartie foreiGn KEY (idPartie ) references Parties(idPartie)
)



CREATE SEQUENCE idPartie_ start with 5;




delete from billes where numero !=0;
delete from coups where ordre != 1000;
delete from solution where idpartie !=1000;


insert into Joueur (Identifiant , Mdp ,DateInscription) values ('Bryan' , 'kirikou' , '11/09/1999');
insert into Joueur (Identifiant , Mdp ,DateInscription) values ('Br' , 'kirikou' ,SYSDATE);
insert into joueur values ('yaya','yrryhrhurjnur',sysdate);
insert into Joueur (Identifiant , Mdp ,DateInscription) values ('Bran' , 'kirikou' , TO_DATE('01-01-2004 13:38:11','DD-MM-YYYY HH24:MI:SS'));

select TO_CHAR(DateInscription,'DD-MM-YYYY HH24:MI:SS') from Joueur where Identifiant ='Br';


INSERT INTO Parties values (1,2,'11/09/1999',1,'yaya',3);

INSERT INTO DIFFICULTE (Niveau,TailleX,TailleY,NbreCouleurs) values (1,4,10,4);
INSERT INTO DIFFICULTE (Niveau,TailleX,TailleY,NbreCouleurs) values (2,4,8,6);
INSERT INTO DIFFICULTE (Niveau,TailleX,TailleY,NbreCouleurs) values (3,5,8,8);

INSERT INTO Billes values ('rouge' , 'heyyy',3);
INSERT INTO Billes values ('vert' , 'heyyy',1);
INSERT INTO Billes values ('bleu' , 'heyyy',2);
INSERT INTO Billes values ('noir' , 'heyyy',4);

INSERT INTO Billes values ('blanc' , 'heyyy',5);

INSERT INTO Billes values ('rose' , 'heyyy',6);
INSERT INTO Billes values ('marron' , 'heyyy',7);
INSERT INTO Billes values ('violet' , 'heyyy',8);


insert into coups values (3,3,1,1,1,'rouge');
insert into coups values (3,3,2,1,1,'rouge');
insert into coups values (3,3,3,1,1,'rouge');
insert into coups values (3,3,4,1,1,'rouge');
insert into coups values (3,3,5,1,1,'bleu');
insert into coups values (3,3,6,1,1,'vert');


insert into coups values (4,1,8,1,69,'bleu');
insert into coups values (4,2,5,2,69,'bleu');
insert into coups values (4,3,6,3,69,'rose');
insert into coups values (4,4,7,4,69,'vert');
insert into coups values (4,1,9,5,69,'bleu');
insert into coups values (4,1,10,6,69,'rose');
insert into coups values (4,2,11,7,69,'rose');
insert into coups values (4,3,12,8,69,'vert');



insert into coups values (5,1,20,20,69,'bleu');
insert into coups values (5,2,21,21,69,'bleu');
insert into coups values (5,3,22,22,69,'rose');


insert into coups values (6,4,13,4,69,'bleu');
insert into coups values (6,1,14,5,69,'bleu');
insert into coups values (6,1,15,6,69,'rose');
insert into coups values (6,2,16,7,69,'rose');
insert into coups values (6,3,17,8,69,'vert');

select * from solution where idpartie = 69;

CREATE OR REPLACE PROCEDURE Insert_Joueur(idJ Joueur.Identifiant%type ,mdpJ Joueur.mdp%type,retour out number) IS 
nbredoublon number (2);
doublon exception ; 
taillemdp exception ;

    BEGIN
retour :=0;
Select count(*) into nbredoublon from sbc2937a.Joueur where Identifiant =idJ ; 

if nbredoublon >0 then raise 
doublon ;
end if ; 

if LENGTH(mdpJ)<6 then raise taillemdp;
end if ;
insert into sbc2937a.Joueur (Identifiant,mdp,Dateinscription) values (idJ,mdpJ,sysdate);
commit;

    EXCEPTION 
when doublon then  retour :=1 ;
when taillemdp then retour :=2;
when others then retour:= SQLCODE;
end ;
/



declare
a number(10);

begin
INSERT_JOUEUR('bree','braaaa',a);
DBMS_OUTPUT.PUT_LINE(a);
end;
 /


select * from joueur;




Create or replace Procedure replay_game(numPartie Parties.IdPartie%type,retour out number, c1 out SYS_REFCURSOR )
is
nbrePartie number(2);
PartieInexistante exception; 

BEGIN  
open c1 for  SELECT * from sbc2937a.Coups where IDPARTIE = numPartie order by ordre DESC;
retour := 0 ; 

select count(*)into nbrePartie from sbc2937a.Parties where idPartie = NumPartie ; 

if nbrePartie != 1 then raise PartieInexistante ; 
end if ; 



EXCEPTION 
when PartieInexistante then retour := 1 ; 
when others then retour:= SQLCODE;
end;
/





DECLARE 
  c1 SYS_REFCURSOR;
  n number(2);
  temp_dbuser coups%ROWTYPE;
BEGIN
 
  --records are assign to cursor 'c_dbuser'
  replay_game(1,n,c1);
  LOOP
 --fetch cursor 'c_dbuser' into dbuser table type 'temp_dbuser'
        FETCH c1 INTO temp_dbuser;
 --exit if no more records
        EXIT WHEN c1%NOTFOUND;

        --print the matched username
        dbms_output.put_line(temp_dbuser.ligne);
 
  END LOOP;
 
  CLOSE c1;
 
END;
/





create or replace procedure creerPartie ( IdJ Joueur.IDENTIFIANT%type,nivP DIFFICULTE.niveau%type,idparr out number,retour out number ) is
niveauInexistant_ exception ; 
niveauexistant number;
tailleX_ number;
Nombrecoul number;
iterateur number;
numCouleur number;
nomCouleur billes.couleur%type;
idp number := idpartie_.nextval;

BEGIN
retour :=2 ;
select count(*)into niveauexistant from sbc2937a.difficulte where niveau = nivp ;
if niveauexistant = 0 then raise niveauInexistant_;
end if;
insert into sbc2937a.PARTIES ( idpartie,date_partie,IdentifiantP,NiveauP) values (idp,Sysdate,Idj,nivP);

select tailleX into tailleX_  from sbc2937a.DIFFICULTE where niveau = nivP;
select nbrecouleurs into Nombrecoul  from sbc2937a.DIFFICULTE where niveau = nivP;


For i in 1..tailleX_
    Loop
        SELECT ceil (dbms_random.value(0, Nombrecoul)) into numCouleur
        FROM dual;
         
         select couleur into nomCouleur from Billes where numero = numCouleur;
        insert into  sbc2937a.Solution values (nomCouleur,idp,i);
        
    End loop ;
retour :=0;
idparr := idp;
commit ; 

exception 
when niveauInexistant_ then
retour :=1 ;


end;
/



DECLARE
retour number(10);
idpar number;

BEGIN 
creerPartie('Bran',2,idpar,retour);
DBMS_OUTPUT.PUT_LINE(retour);
DBMS_OUTPUT.PUT_LINE(idpar);
end;
 /


select * from solution where idpartie =70;

select ordre,couleurb from coups where idpartie =69 ;
















create or replace procedure UpdatePartie(idpar parties.idpartie%type , scoreP Parties.Score%type,retour out number)is
doublon number;
AucunCoups exception;
Gagne number;
retour2 number;

BEGIN
PartieGagne(idpar,Gagne,retour2);
if retour2 = 1 then raise
AucunCoups;
end if ;
update Parties set score = scoreP , etat = Gagne where idpartie=idpar;
retour :=0;
commit;


EXCEPTION 
WHEN AucunCoups 
then retour :=1;

end;
/





declare
retour number;

begin
UpdatePartie(70,300,retour);


end;
/





CREATE OR REPLACE PROCEDURE DerniereLigne(idpar parties.idpartie%type,c1 out SYS_REFCURSOR ,retour out number ) IS
AucunCoups exception;
nbrecoups number;

BEGIN
select count(*) into nbrecoups from coups where idpartie = idpar    ;

if nbrecoups <1 then 
raise AucunCoups;
end if ;

open c1 for  select max(ordre),colonne from coups where idpartie = idpar and ligne = (select max(ligne) from coups where idpartie = idpar )  group by colonne   ;
retour :=0;
exception 
when AucunCoups 
then retour :=1;

END;
/

DECLARE 
c1 SYS_REFCURSOR;
n number(2);
TYPE coupsR IS RECORD (
  
    ordre         coups.ordre%type,
    colonne       coups.colonne%type);
    recordCoups coupsR;
        
BEGIN
 DerniereLigne(69,c1,n);
LOOP
    FETCH c1 INTO recordCoups;
    EXIT WHEN c1%NOTFOUND;
    dbms_output.put_line( ' Ordre : ' ||recordCoups.ordre ||' Colonne : ' ||recordCoups.colonne );
END LOOP;
 
CLOSE c1;
  
 
END;
/


CREATE OR REPLACE PROCEDURE PartieFinie(idpar parties.idpartie%type,partieFinie out number ,retour out number ) IS
nbreReponseJ number :=0;
nbreReponseT number;
derniereligneJ number;
nbreLigneT number ; 
retour2 number;
TYPE coupsR IS RECORD (
    ordre         coups.ordre%type,
    colonne       coups.colonne%type);
    recordCoups coupsR;
c1 SYS_REFCURSOR;


begin
DerniereLigne(idpar,c1,retour2);

LOOP
    FETCH c1 INTO recordCoups;
    EXIT WHEN c1%NOTFOUND;
    null;
END LOOP;
select ligne into derniereligneJ from coups where idpartie=idpar and ordre = recordCoups.ordre ; 
 nbreReponseJ := c1%ROWCOUNT;
CLOSE c1;
select TAILLEY into nbreLigneT from difficulte d , parties p  where p.niveaup = d.niveau and idpartie=idpar ; 
select TAILLEX into nbreReponseT from difficulte d , parties p  where p.niveaup = d.niveau and idpartie=idpar ; 

if nbreReponseT = nbreReponseJ and derniereligneJ = nbreLigneT then
partieFinie :=1;
else
partieFinie:=0;
end if ;

null ;

retour :=1;
end;
/



declare
retour number ;
fin number; 



begin 
PartieFinie(69,fin,retour);
 dbms_output.put_line(fin);


end;
/







create or replace procedure PartieGagne ( idpar parties.idpartie%type,partieGG out number ,retour out number ) is
AucunCoups exception ;
couleureS varchar(30);
CouleureC varchar(30);
nbreBonneReponse number :=0 ; 
nombreReponse number;
/* d�claration de record pour permettre d'appeler la proc�dure derniere ligne qui permetra de comparer la solution avec la derni�re ligne rmeplie */
TYPE coupsR IS RECORD (
    ordre         coups.ordre%type,
    colonne       coups.colonne%type);
    recordCoups coupsR;
    
c1 SYS_REFCURSOR; 
retour2 number;

begin 
DerniereLigne(idpar,c1,retour2);
if retour2 =1 then 
raise AucunCoups;
end if ;
LOOP
    FETCH c1 INTO recordCoups;

    EXIT WHEN c1%NOTFOUND;
    select couleurs into couleureS from solution where idpartie=idpar and ordre = recordCoups.colonne;
    select couleurB into CouleureC from coups where idpartie = idpar and ordre = recordCoups.ordre;
    if couleureS = CouleureC then
   nbreBonneReponse := nbreBonneReponse+1 ;
    end if ; 
END LOOP;
 
CLOSE c1;

select count(*)into nombreReponse from solution where idpartie = idpar;

if nombreReponse = nbreBonneReponse then
partieGG := 1;
else
partieGG :=0;
end if ; 

exception
when AucunCoups then
retour :=2;

end;
/




declare
retour number;
gg number;

begin
PartieGagne(70,gg,retour);
dbms_output.put_line(gg);
end;
/






CREATE OR REPLACE PROCEDURE JouerCoup ( idpar parties.idpartie%type,ligneC coups.ligne%type,colonneC coups.colonne%type,Couleurcb coups.couleurB%type,tempC coups.temps%type,retour out number) is 
DernierCoup number ; 
LigneInterdite exception ;
AucunCoups exception ;
TailleXP number ;
nbreReponseJ NUMBER;

TYPE coupsR IS RECORD (
    ordre         coups.ordre%type,
    colonne       coups.colonne%type);
    recordCoups coupsR;
    
c1 SYS_REFCURSOR; 
retour2 number;
derniereligneJ number;



begin




select max(ordre) into DernierCoup from coups where idpartie = idpar ; 


if DernierCoup is  null then
if ligneC = 1 then
dbms_output.put_line('dernier coup'  );
insert into coups values (ligneC,colonneC,1,tempC,idpar,Couleurcb);
else 
raise LigneInterdite;
end if ;
else

DerniereLigne(idpar,c1,retour2);
select tailleX into TailleXP from difficulte d , parties p where p.niveaup = d.niveau and idpartie = idpar ; 


LOOP
    FETCH c1 INTO recordCoups;
    EXIT WHEN c1%NOTFOUND;
    null;
END LOOP;
select ligne into derniereligneJ from coups where idpartie=idpar and ordre = recordCoups.ordre ; 
 nbreReponseJ := c1%ROWCOUNT;
CLOSE c1;

if derniereligneJ = ligneC or (ligneC = derniereligneJ+1 and nbreReponseJ = TailleXP)  then
insert into coups values (ligneC,colonneC,DernierCoup+1,tempC,idpar,Couleurcb);
else 
raise LigneInterdite;
end if ;
end if ;
retour := 0;
commit ; 
exception
when LigneInterdite then
retour :=1;
when AucunCoups then
retour :=2;

end;
/



declare 
retour number ;
begin
JouerCoup(94,1,4,'noir',5,retour);
dbms_output.put_line(retour );



end;
/


select * from coups where idpartie=94;
delete from coups where idpartie=94;






select to_date(date_fin, 'hh24:mi:ss') - to_date(date_debut, 'hh24:mi:ss') from ma_table;



select * from parties where identifiantp ='yaya' and date_partie >= SYSDATE-(5/24) order by date_partie asc ;


INSERT INTO parties values (179,300,Sysdate,0,'yaya',3);


delete from parties where identifiantp ='yaya' and date_partie > Sysdate-(4/24) and etat =0;


delete from coups where identifiantp ='yaya';
ALTER TRIGGER LimitationPartie disable;


CREATE OR REPLACE TRIGGER LimitationPartie
BEFORE INSERT on parties 
for each row 

declare 
cursor c1  is select * from parties where IDENTIFIANTP = :new.identifiantp and date_partie >= ( :new.date_partie- (5/24 )) and etat=0 ;
nbreParties number ;
dateDernierePartie date;
listepartie parties%rowtype;



begin


for listepartie in c1 loop
 
 select count(*),max(date_partie) into nbreParties,dateDernierePartie from parties where identifiantP= :new.identifiantP and date_partie between listepartie.date_partie and listepartie.date_partie+(1/24) and etat=0 ;
    
    if nbreParties>= 5 AND (:new.date_partie-dateDernierePartie) <4/24 then
    RAISE_APPLICATION_ERROR(-20001,'Blocage encore actif pendant '|| to_char(trunc((4/24-(:new.date_partie-dateDernierePartie))*24*60)+1 ||' minutes') );
    end if ; 
    

end loop ;
end;
/


 TO_DATE('01-01-2004 13:38:11','DD-MM-YYYY HH24:MI:SS');


select count(*) from parties where IDENTIFIANTP = 'yaya' and date_partie >= (SYSDATE- (4/24 ))  ;







ALTER TRIGGER NiveauAccessible disable;

CREATE OR REPLACE TRIGGER NiveauAccessible
BEFORE INSERT on parties 
for each row 

declare
MeilleurNiveau number;
begin

select max(niveaup) into MeilleurNiveau from parties where identifiantp = :new.identifiantp and etat=1;
if MeilleurNiveau is null and :new.niveaup !=1 then
 RAISE_APPLICATION_ERROR(-20002,'Niveau pas encore d�bloqu�e ') ;
end if ; 

if  MeilleurNiveau is not null and :new.niveaup>MeilleurNiveau+1 then
RAISE_APPLICATION_ERROR(-20002,'Niveau pas encore d�bloqu�e ') ;

end if ; 
end;
/

select * from parties where identifiantp = 'br'; 




insert into parties values(194,300,SYSDATE,1,'br',3);


delete from parties where identifiantp = 'br';


declare
dt date := SYSDATE;

dt2 date :=   TO_DATE('01-01-2004 13:38:11','DD-MM-YYYY HH24:MI:SS');
begin


if dt-dt2>0 then
dbms_output.put_line('positif' );
end if ; 

end;
/






to_date(date_fin, 'hh24:mi:ss');





select * from parties where identifiantp = 'yaya';


select count(*) from parties where identifiantP  =  'yaya' and to_date(Date_PARTIE,'DD-MM-YYYY') = to_date(SYSDATE,'DD-MM-YYYY') and to_number(to_char(date_partie,' HH24'))=17;



select (date2-date1)*24 hours;
select (date2-date1)*1440 MINUTES
select (date2-date1)*86400 seconds